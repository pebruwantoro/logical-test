package main

import "fmt"

func main() {
	fmt.Println(PrimeNumber(11, 40))
}

func PrimeNumber(x, y int) []int {
	var result []int
	for i := x; i <= y; i++ {
		prime := 0
		for j := 1; j <= y; j++ {
			if i%j == 0 {
				prime++
			}
		}
		if prime == 2 {
			result = append(result, i)
		}
	}
	return result
}
