package main

import (
	"fmt"
	"strconv"
)

func main() {
	var arr []string = []string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}
	fmt.Println(CountSameElement(arr))
}

func CountSameElement(input []string) [][]string {
	var result [][]string
	var check []string
	element := 1
	for i := 1; i < len(input); i++ {
		if input[i-1] == input[i] {
			element++
		}
		if input[i-1] != input[i] {
			str := strconv.Itoa(element)
			check := append(check, str)
			check = append(check, input[i-1])
			result = append(result, check)
			element = 1
		}
		if i == len(input)-1 {
			element = 1
			for j := len(input) - 1; j >= 1; j-- {
				if input[j] == input[j-1] {
					element++
				}
				if input[j] != input[j-1] {
					str := strconv.Itoa(element)
					check := append(check, str)
					check = append(check, input[j])
					result = append(result, check)
					break
				}
			}
		}
	}
	return result
}
