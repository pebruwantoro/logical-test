package main

import "fmt"

func main() {
	var arr []string = []string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}
	fmt.Println(SubArrayGroup(arr))
}

func SubArrayGroup(input []string) [][]string {
	var result [][]string
	var sub_array []string
	for i := 1; i < len(input); i++ {
		if input[i-1] == input[i] {
			sub_array = append(sub_array, input[i-1])
		}
		if input[i-1] != input[i] {
			sub_array = append(sub_array, input[i-1])
			result = append(result, sub_array)
			sub_array = []string{}
		}
		if i == len(input)-1 {
			if input[i-1] == input[i] {
				sub_array = append(sub_array, input[i])
			}
			if input[i-1] != input[i] {
				sub_array = append(sub_array, input[i])
			}
			result = append(result, sub_array)
		}
	}
	return result
}
