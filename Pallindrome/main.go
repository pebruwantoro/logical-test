package main

import (
	"fmt"
)

func main() {
	fmt.Println(palindrome("abcba"))
}

func palindrome(input string) bool {
	reverse_string := ""
	for i := len(input) - 1; i >= 0; i-- {
		reverse_string += string(input[i])
	}
	if reverse_string == input {
		return true
	}
	return false
}
